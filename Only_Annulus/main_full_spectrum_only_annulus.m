    %% WORKSPACE INITIALIZATION
   
    clc;
    clear all;
    close all;
    addpath './pcheb'
    
    % ---------- %
    % User input %
    % ---------- %  
    
    % factor (only odd numbers)
    fac = 3; 

    % Annulus Geometry

    global n;      n      = 4*fac + 1;                               % Number of interior Top velocity points make this even
    global m;      m      = 8*fac;                               % Number of points along theta axis
    global R2;     R2     = 4;                       % Outer Radius in meters
    global R1;     R1     = 3;                         % Inner Radius in meters
    global L;      L      = R2-R1;                            % Length of annular sector
    global S;      S      = 16;                               % Number of sectors
    global theta;  theta  = 2*pi/S;                           % Angle of sector
    global dth;   dth     = theta/m;                          % dth

    % Thermodynamic properties
    global gamma; gamma = 1.4;
    global Ma;    Ma    = 0.05;

    % ----------------- % 
    % Global parameters %
    % ----------------- %

    global iTV;    iTV    = 3;                               % Velocity stored above
    global iLV;    iLV    = 2;                               % Velocity stored to the left
    global iP;     iP     = 1;                               % Pressure stored at center
    global sh;     sh     = -1;                              % Shift (matlab indexing)
    global pade_; pade_   = 1.0/24.0;                        % Off-diagonal coefficient
    
    % radial, theta and x co-ordinates
    
        % radial vectors 
            % Top Velocitoes co-ordinates (n-1)
            global r_vec; r_vec1 = (R2-R1).*(linspace(0,1,2*n+1)) + R1.*(ones(1,2*n+1)); r_vec11 = r_vec1(1,1:2:2*n+1); r_vec = r_vec11(1,2:end-1);
            % Pressure co-ordinates (n)
            global r_vec_p; r_vec_p = r_vec1(1, 2:2:end); 
            global dr; dr = r_vec_p(2)-r_vec_p(1); 
        
        % theta vector
        global theta_vec; theta_vec = linspace(0, theta-dth, m); % Theta co-ordinates

    % The total number of distinct quantities that ought to be calculated inside the Disc/Annulus
    Y    = zeros(1, (3*n-1)*m);  
   
    
    %% Constant matrices in the code
    
    global cm
    global cm2
    global cmn
    tic
    [cm, cm2, cmn] = eV_mat_gauss_v_a_a_v_only_annulus(Y);
    
    disp('Assembly matrix done, after');
    toc
    disp('============================');
    
    
    %% Creates required directories, files and clears file_list
    
    if(exist(['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m),'/'],'dir')==0)
        mkdir(['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m),'/']);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/create_full_pressure.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/create_full_pressure_sim.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/merge.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/polarplot3d.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/simulate_vectors.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/analytical_sol_only_annulus.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/Apply_analytical_result.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/Apply_analytical_result_0.m',['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)]);
    end
    fid=fopen(['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m),'/file_list.txt'],'w');

    %% COMPUTING THE EIGEN VALUES/VECTORS FOR A SECTOR   
    for i=1:7
        [EV_Set, EVa_Set] = eigenVSolve(i-1); % Tube code is confirmed and 2D annulus is confirmed
        EV_af = diag(EVa_Set);
        rou = i-1;
        % Save eigen right eigen vector matrix and eigen value vector        
        save(['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m),'/j',num2str(i-1),'_Rr_',num2str(R1/R2),'_EV_Set_EV_af.mat'],...
            'EV_Set','EV_af','S','n','theta','m','R1','R2','r_vec_p','theta_vec','rou'); 

        if(i == 1)
            fprintf(fid,'%s',['j',num2str(i-1),'_Rr_',num2str(R1/R2),'_EV_Set_EV_af.mat']);
        else
            fprintf(fid,'\n%s',['j',num2str(i-1),'_Rr_',num2str(R1/R2),'_EV_Set_EV_af.mat']);  
        end
        disp(['==========EigenVectors and EigenValues of j = ',int2str(i-1),' saved==================']);               
    end
    fclose(fid);
    
    disp('Single sector results');
    
%     pa = ['./results/eigen_spectrum/Sectors=',int2str(S),'/Rr=',num2str(R1/R2),'/n=',num2str(n),'/m=',num2str(m)];
%     
%     main_full_spectrum_only_annulus_1_Sector;