    %% WORKSPACE INITIALIZATION
   
    clc;
    clearvars -except pa;
    addpath './pcheb'
    
    % ---------- %
    % User input %
    % ---------- %  
    
    % factor (only odd numbers)
    fac = 1; 

    % Annulus Geometry

    global n;      n      = 9*fac;                               % Number of interior Top velocity points make this even
    global m;      m      = 8*16*fac;                               % Number of points along theta axis
    global R2;     R2     = 4;                       % Outer Radius in meters
    global R1;     R1     = 3;                         % Inner Radius in meters
    global L;      L      = R2-R1;                            % Length of annular sector
    global S;      S      = 1;                               % Number of sectors
    global theta;  theta  = 2*pi/S;                           % Angle of sector
    global dth;   dth     = theta/m;                          % dth

    % Thermodynamic properties
    global gamma; gamma = 1.4;
    global Ma;    Ma    = 0.05;

    % ----------------- % 
    % Global parameters %
    % ----------------- %

    global iTV;    iTV    = 3;                               % Velocity stored above
    global iLV;    iLV    = 2;                               % Velocity stored to the left
    global iP;     iP     = 1;                               % Pressure stored at center
    global sh;     sh     = -1;                              % Shift (matlab indexing)
    global pade_; pade_   = 1.0/24.0;                        % Off-diagonal coefficient
    
    % radial, theta and x co-ordinates
    
        % radial vectors 
            % Top Velocitoes co-ordinates (n-1)
            global r_vec; r_vec1 = (R2-R1).*(linspace(0,1,2*n+1)) + R1.*(ones(1,2*n+1)); r_vec11 = r_vec1(1,1:2:2*n+1); r_vec = r_vec11(1,2:end-1);
            % Pressure co-ordinates (n)
            global r_vec_p; r_vec_p = r_vec1(1, 2:2:end); 
            global dr; dr = r_vec_p(2)-r_vec_p(1); 
        
        % theta vector
        global theta_vec; theta_vec = linspace(0, theta-dth, m); % Theta co-ordinates

    % The total number of distinct quantities that ought to be calculated inside the Disc/Annulus
    Y    = zeros(1, (3*n-1)*m); 
   
    
    %% Constant matrices in the code
    
    global cm
    global cm2
    global cmn
    tic
    [cm, cm2, cmn] = eV_mat_gauss_v_a_a_v_only_annulus(Y);
    
    disp('Assembly matrix done, after');
    toc
    disp('============================');
    
    %% Creates required directories, files and clears file_list
    
    fid=fopen([pa,'/file_list.txt'],'a');

    %% COMPUTING THE EIGEN VALUES/VECTORS FOR A SECTOR
    S_1 = S;
    n_1 = n;
    theta_1 = theta;
    m_1 = m;
    R1_1 = R1;
    R2_1 = R2;
    r_vec_p_1 = r_vec_p;
    theta_vec_1 = theta_vec;
    
    i=1;
        [EV_Set, EVa_Set] = eigenVSolve(i-1); % Tube code is confirmed and 2D annulus is confirmed
        EV_af = diag(EVa_Set);

        % Save eigen right eigen vector matrix and eigen value vector        
        save([pa,'/EV_Set_EV_af_1_Sector.mat'],...
            'EV_Set','EV_af','S_1','n_1','theta_1','m_1','R1_1','R2_1','r_vec_p_1','theta_vec_1'); 

        fprintf(fid,'\n%s','EV_Set_EV_af_1_Sector.mat');
        disp('==========EigenVectors and EigenValues of full annulus saved==================');               
    
    fclose(fid);
    
    disp('===========DONE==============');