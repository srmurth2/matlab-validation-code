% function couples the Disc/Annulus with the Rijke tube in a volume-area; area-volume manner

function [A, A2, An] = eV_mat_gauss_v_a_a_v_only_annulus(Y) 

 % necessary variables
  global n m
  global sh iTV iLV iP 
  global dth R2 R1
  global gamma Ma
  global r_vec_p r_vec
  
  c1 = 1/(gamma*Ma); %14.28
  c2 = (gamma*Ma); %0.07
  
  Y_size = (3*n-1)*m;
  
 %% Disc/Annulus matrix

      A  = zeros(Y_size,Y_size);
      A2 = zeros(Y_size,Y_size);
      An = zeros(Y_size,Y_size);

      % Top Velocities
      for j=1:m

          for i=1:n-1
              dR = r_vec_p(i+1)-r_vec_p(i);
              A(3*(i+sh)+iTV+(3*n-1)*(j+sh),3*(i+sh)+iP+(3*n-1)*(j+sh)) = c1/dR;
              A(3*(i+sh)+iTV+(3*n-1)*(j+sh),3*(i+1+sh)+iP+(3*n-1)*(j+sh)) = -c1/dR;
          end

      end

      % Left Velocities
      for i=1:n
          for j=1:m-1
            A(3*(i+sh)+iLV+(3*n-1)*(j+sh), 3*(i+sh)+iP+(3*n-1)*(j+sh)) = c1/ (dth*r_vec_p(i));
            A(3*(i+sh)+iLV+(3*n-1)*(j+sh), 3*(i+sh)+iP+(3*n-1)*(j+1+sh)) = -c1/ (dth*r_vec_p(i));
          end
          j = m;
          A(3*(i+sh)+iLV+(3*n-1)*(j+sh), 3*(i+sh)+iP+(3*n-1)*(j+sh)) =  c1/(dth*r_vec_p(i));
          A2(3*(i+sh)+iLV+(3*n-1)*(j+sh), 3*(i+sh)+iP+(3*n-1)*(1+sh)) = -c1/(dth*r_vec_p(i)); % fills the first coloumn
      end
      % Center Pressures part 1
      
      j=1;
          i=1;
          dR = r_vec(i)-R1;
          A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iTV+(3*n-1)*(j+sh)) = -c2*(r_vec(i)) / (dR*r_vec_p(i));
          
           for i=2:n-1
             dR = r_vec(i)-r_vec(i-1);
             A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh-1)+iTV+(3*n-1)*(j+sh)) = c2*(r_vec(i-1)) / (dR*r_vec_p(i));
             A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iTV+(3*n-1)*(j+sh)) = -c2*(r_vec(i)) / (dR*r_vec_p(i));

           end
           
           i=n;
           dR = R2-r_vec(i-1);
           A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh-1)+iTV+(3*n-1)*(j+sh)) = c2*(r_vec(i-1)) / (dR*r_vec_p(i));
      
      for j=2:m-1
          i=1;
          dR = r_vec(i)-R1;
          A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iTV+(3*n-1)*(j+sh)) = -c2*(r_vec(i)) / (dR*r_vec_p(i));
          
           for i=2:n-1
             dR = r_vec(i)-r_vec(i-1);
             A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh-1)+iTV+(3*n-1)*(j+sh)) = c2*(r_vec(i-1)) / (dR*r_vec_p(i));
             A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iTV+(3*n-1)*(j+sh)) = -c2*(r_vec(i)) / (dR*r_vec_p(i));

           end
           
           i=n;
           dR = R2-r_vec(i-1);
           A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh-1)+iTV+(3*n-1)*(j+sh)) = c2*(r_vec(i-1)) / (dR*r_vec_p(i));
      end
      
      j=m;
          i=1;
          dR = r_vec(i)-R1;
          A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iTV+(3*n-1)*(j+sh)) = -c2*(r_vec(i)) / (dR*r_vec_p(i));
          
           for i=2:n-1
             dR = r_vec(i)-r_vec(i-1);
             A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh-1)+iTV+(3*n-1)*(j+sh)) = c2*(r_vec(i-1)) / (dR*r_vec_p(i));
             A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iTV+(3*n-1)*(j+sh)) = -c2*(r_vec(i)) / (dR*r_vec_p(i));
             
           end
           
           i=n;
           dR = R2-r_vec(i-1);
           A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh-1)+iTV+(3*n-1)*(j+sh)) = c2*(r_vec(i-1)) / (dR*r_vec_p(i));
      
      
      % Center Pressures part 2      
      for i=1:n
          j=1;
          An(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iLV+(3*n-1)*(m+sh)) = c2/(dth*r_vec_p(i));  % fills the last coloumn
          A(3*(i+sh)+iP+(3*n-1)*(j+sh), 3*(i+sh)+iLV+(3*n-1)*(j+sh)) = -c2/(dth*r_vec_p(i));
          for j=2:m
            A(3*(i+sh)+iP+(3*n-1)*(j+sh),3*(i+sh)+iLV+(3*n-1)*(j-1+sh)) = c2/(dth*r_vec_p(i));
            A(3*(i+sh)+iP+(3*n-1)*(j+sh),3*(i+sh)+iLV+(3*n-1)*(j+sh)) = -c2/(dth*r_vec_p(i));
          end
      end