function An_Eva_OA = analytical_sol_only_annulus(interval, beta, R2, R1)
    
    i1 = interval(1);
    ie = interval(end);
    delta = 0.1;
    NOI = floor((ie-i1)/delta);
    An_Eva_OA = [];
    for noi = 1:NOI 
    
        if ((sign(OA_fun(i1, beta, R2, R1))~=sign(OA_fun(interval(1)+(noi)*delta, beta, R2, R1))) && (interval(1)+(noi)*delta ~= 0))
            fun = @(lambda) OA_fun(lambda, beta, R2, R1);                
            An_Eva_OA = [An_Eva_OA, fzero(fun, [i1,interval(1)+(noi)*delta])];
            i1 = An_Eva_OA(end) + 0.00005;
        end
    end
    
    An_Eva_OA = sqrt(-1)*An_Eva_OA;

end

function y = OA_fun(lambda, beta, R2, R1)
    if(beta ~= 0)
        y = real( ((bessely(beta-1,lambda*R1) - bessely(beta+1,lambda*R1)))*(besselj(beta-1,lambda*R2) - besselj(beta+1,lambda*R2)) ...
            - (besselj(beta-1,lambda*R1) - besselj(beta+1,lambda*R1))*(bessely(beta-1,lambda*R2) - bessely(beta+1,lambda*R2)));
    else
        y = real((bessely(1, lambda * R2)*besselj(1, lambda * R1) - bessely(1, lambda * R1)*besselj(1, lambda * R2)));
    end

end

% plot(OA_fun(0:0.001:70, beta, R2, R1));