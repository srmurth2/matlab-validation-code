%=============================================================================================================================
% This script merges all MATLAB data in the current folder using the file names
% present in the file_list.txt file
% REQUIREMENTS : 
% This script must exist in a folder containing
%   1. polarplot3d                                                                (polar ploting)
%   2. create_full_pressure                                                       (constructs the full solution from the sector eigen vectors)
%   3. file_list.txt                                                              (file containing a list of all the MATLAB DATA files)
%   4. MATLAB DATA files with Eigen Vector and Eigen Value data for all j values  (all MATALB DATA files)
%=============================================================================================================================

clc;
close all;
clear all;
close all;
n_rj = 0;
w_t_l = input('Which j value to load, press enter for all : ');

if(~isempty(w_t_l))

file_names = importdata('file_list.txt');
disp(['Loading of j',num2str(w_t_l),': Started']);
tic
load(char(file_names(w_t_l+1)));
toc
EV_Set_all = EV_Set;
EV_af_all  = EV_af;
num_eva_per_j = length(EV_af);
num_j = 1;

c = ['b';'r';'k';'m';'y';'b';'c';'g'];
shape = ['o';'s';'d';'+';'*';'<';'v';'x'];
alphabet = 'z';
trial=1;
while(alphabet == 'z')
    fig1=figure(1);
    if(trial == 1)
        hold on;
        grid on;
        for j = 1:num_j
            if(j<=7)
                plot(real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),[shape(mod(j-1,8)+1,1),c(mod(j-1,8)+1,1)]);
           
            else
                plot(real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),[shape(8-mod(j-1,8),1),c(mod(j-1,8)+1,1)]);
            end

        end
        axis([-1 1 5 10])
        xlabel('Eigen Spectrum');
        hold off;
        trial=2;
    end

    disp('Select the appropriate region of interest then press Enter');
    pause;
    alphabet = 's';
    while(alphabet == 's')


Dsel    = ginput(1);                        %..see ginput. [x, y] value selected
Dsel    = Dsel(1) + 1i*Dsel(2);             %..putting result into complex value

Ddiff1   = abs(EV_af_all - Dsel);                   %..Distance from selected value to each eigenvalue
[~, II1] = min(Ddiff1);                       %..determine minimum distance to find the nearest eigenvalue
Dval1    = EV_af_all(II1);                           %..Assign selected eigenvalue to Dval (output

Dsel2    = ginput(1);                        %..see ginput. [x, y] value selected
Dsel2    = Dsel2(1) + 1i*Dsel2(2);             %..putting result into complex value

Ddiff2   = abs(EV_af_all + Dsel2);                   %..Distance from selected value to each eigenvalue
[~, II2] = min(Ddiff2);                       %..determine minimum distance to find the nearest eigenvalue
Dval2    = EV_af_all(II2);                           %..Assign selected eigenvalue to Dval (output

i = w_t_l+1;

V1    = EV_Set_all(:, II1);                   %..Find the eigenvector of the corresponding eigenvalue
V2    = EV_Set_all(:, II2);

V111 = create_full_pressure(V1((2*(n_rj)+1):end),S,i-1,m,'r') + sqrt(-1)*create_full_pressure(V1((2*(n_rj)+1):end),S,i-1,m,'i');
V121 = create_full_pressure(V2((2*(n_rj)+1):end),S,i-1,m,'r') + sqrt(-1)*create_full_pressure(V2((2*(n_rj)+1):end),S,i-1,m,'i');

V11 = V111(4,:);
V12 = V121(4,:);

fig2 = figure(2);
subplot(3,2,1);
plot(abs(V11), '-ob');

subplot(3,2,2);
plot(angle(V11), '-or');

subplot(3,2,3);
plot(abs(V12), '-ob');

subplot(3,2,4);
plot(angle(V12), '-or');

subplot(3,2,5);
plot(abs(V11+V12), '-ob');

subplot(3,2,6);
plot(angle(V11+V12), '-or');

alphabet = input('z+enter -change region; s+enter -stay on same region; Any Other key+enter -exit\n','s');
close(fig2);

clc;
    end
end
disp('exit');
close all;

else
    disp('ERROR : No selection made');
end

