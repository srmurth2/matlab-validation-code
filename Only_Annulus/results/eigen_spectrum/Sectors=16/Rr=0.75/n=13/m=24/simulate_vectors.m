%=============================================================================================================================
% This script merges all MATLAB data in the current folder using the file names
% present in the file_list.txt file
% REQUIREMENTS : 
% This script must exist in a folder containing
%   1. polarplot3d                                                                (polar ploting)
%   2. create_full_pressure                                                       (constructs the full solution from the sector eigen vectors)
%   3. file_list.txt                                                              (file containing a list of all the MATLAB DATA files)
%   4. MATLAB DATA files with Eigen Vector and Eigen Value data for all j values  (all MATALB DATA files)
%=============================================================================================================================

clc;
close all;
clear all;

file_names = importdata('file_list.txt');
disp('Loading data, this will take a few minutes');
disp('Loading j0 data: Started');
tic
load(char(file_names(1)));
toc
disp('Loading j0 data: Complete');
EV_Set_all = EV_Set;
EV_af_all  = EV_af;
num_eva_per_j = length(EV_af);
for j = 2:length(file_names)
    disp(['Loading j',num2str(j-1),' data: Started']);
    load(char(file_names(j)));
    disp(['Loading j',num2str(j-1),' data: Complete']);
    EV_Set_all = [EV_Set_all, EV_Set];
    EV_af_all  = [EV_af_all; EV_af];
end
g=cp*2;
c = ['b';'r';'k';'m';'y';'b';'c';'g'];
shape = ['o';'s';'d';'+';'*';'<';'v';'x'];
alphabet = 'z';
trial=1;
while(alphabet == 'z')
    fig1=figure(1);
    if(trial == 1)
        for j = 1:length(file_names)
            if(j<=7)
                plot(real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),[shape(mod(j-1,8)+1,1),c(mod(j-1,8)+1,1)]);
            else
                plot(real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),[shape(8-mod(j-1,8),1),c(mod(j-1,8)+1,1)]);
            end
            hold on;
            grid on;
        end
        xlabel('Eigen Spectrum');
        hold off;
        trial=2;
    end

    disp('Select the appropriate region of interest then press Enter');
    pause;
    alphabet = 's';
    while(alphabet == 's')
       
        % First Eigen value
        
        Dsel    = ginput(1);                                 %..see ginput. [x, y] value selected
        Dsel    = Dsel(1) + 1i*Dsel(2);                    %..putting result into complex value

        Ddiff   = abs(EV_af_all - Dsel);                    %..Distance from selected value to each eigenvalue
        [~, II] = min(Ddiff);                               %..determine minimum distance to find the nearest eigenvalue
        Dval    = EV_af_all(II);                            %..Assign selected eigenvalue to Dval (output

        V       = EV_Set_all(:, II);                        %..Find the eigenvector of the corresponding eigenvalue
        
        i       = ceil(II/num_eva_per_j);
        
        ev2 = input('Y -select second eigen value; N -simulate eigen vector of first eigen value\n','s');
        i2 = 0;
        Dval2 = 0;
        if(ev2 == 'Y')
            % Second Eigen value

            Dsel2    = ginput(1);                                 %..see ginput. [x, y] value selected
            Dsel2    = Dsel2(1) + 1i*Dsel2(2);                    %..putting result into complex value

            Ddiff2   = abs(EV_af_all - Dsel2);                    %..Distance from selected value to each eigenvalue
            [~, II2] = min(Ddiff2);                               %..determine minimum distance to find the nearest eigenvalue
            Dval2    = EV_af_all(II2);                            %..Assign selected eigenvalue to Dval (output

            V2       = EV_Set_all(:, II2);                        %..Find the eigenvector of the corresponding eigenvalue

            i2       = ceil(II2/num_eva_per_j);
        end
        
        % Time evolution of the annular eigen vector 
        fig2 = figure(2);
        set(fig2,'Position',[0,0,2300,1300]);
        for t = (0:0.1:5)
            
            % 2D ANNULUS Plot

            subplot(1,2,1);
            % full domain REAL
            if(ev2=='Y')
                full_pressure = create_full_pressure_sim(V((2*(n_rj)-1-g):end-1-g),S,i-1,m,'r',Dval*t) + create_full_pressure_sim(V2((2*(n_rj)-1-g):end-1-g),S,i2-1,m,'r',Dval2*t);
            else
                full_pressure = create_full_pressure_sim(V((2*(n_rj)-1-g):end-1-g),S,i-1,m,'r',Dval*t);
            end
            
            if(t==0)
                cmin = min(min(full_pressure));
                cmax = max(max(full_pressure));
            end
 
            polarplot3d(full_pressure,'plottype','surfn','angularrange',[0 theta*S-theta/(2*m)],'radialrange',[R1 R2],'tickspacing',15);
            caxis([cmin cmax]);
            xlabel(['REAL pressure, J = (',int2str(i-1),', ',int2str(i2-1),') ev1 = ',num2str(Dval),' ev2 = ',num2str(Dval2),' and #sec = ',int2str(S),' with ',int2str(n),' r & ', int2str(m),' th divisions time = ',num2str(t)]);
            view(0,90);
            colorbar;
            axis square
            
            clear full_pressure;

            %   full domain IMAGINARY
            subplot(1,2,2);
            if(ev2=='Y')
                full_pressure = create_full_pressure_sim(V((2*(n_rj)-1-g):end-1-g),S,i-1,m,'i',Dval*t) + create_full_pressure_sim(V2((2*(n_rj)-1-g):end-1-g),S,i2-1,m,'i',Dval2*t);
            else
                full_pressure = create_full_pressure_sim(V((2*(n_rj)-1-g):end-1-g),S,i-1,m,'i',Dval*t);
            end
     
            polarplot3d(full_pressure,'plottype','surfn','angularrange',[0 theta*S-theta/(2*m)],'radialrange',[R1 R2],'tickspacing',15);
            caxis([cmin cmax]);
            xlabel(['IMAG pressure, J = (',int2str(i-1),', ',int2str(i2-1),'), ev1 = ',num2str(Dval),' ev2 = ',num2str(Dval2),' and #sec = ',int2str(S),' with ',int2str(n),' r & ', int2str(m),' th divisions time = ',num2str(t)]);
            view(0,90);
            colorbar;
            axis square
            
            pause(0.005);
        
        end
        %==================================================
        
        % Tube plots
        
        fig3 = figure(3);
        set(fig3,'Position',[0,0,600,600]);
        pow = 0;
        for w = 1:1
            % full domain REAL Rijke tube
            if(w<=7)
                plot(XU.',([0.0; real(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))]...
                /norm([0.0; real(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))])),['-',shape(mod(w-1,8)+1,1),c(mod(w-1,8)+1,1)]);
            else
                plot(XU.',([0.0; real(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))]...
                /norm([0.0; real(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))])),['-',shape(8-mod(w-1,8),1),c(mod(w-1,8)+1,1)]);
            end
            pow = pow+1;
            hold on;

        end
        xlabel('REAL velocity eigen vectors for the tube');
        axis([0.0, L_tube-hdx -1 1]);
        hold off;
        
        % TUBE IMAG 
        fig4 = figure(4);
        set(fig4,'Position',[0,0,600,600]);
        pow = 0;
        for w = 1:1

            % full domain IMAG Rijke tube
            if(w<=7)
                plot(XU.',([0.0; imag(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))]...
                /norm([0.0; imag(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))])),['-',shape(mod(w-1,8)+1,1),c(mod(w-1,8)+1,1)]);
            else
                plot(XU.',([0.0; imag(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))]...
                /norm([0.0; imag(exp(sqrt(-1)*(pow)*(i-1)*2*pi/S)*V([2:2:2*(n_rj)-1-1-g, (end-g:2:end)]))])),['-',shape(8-mod(w-1,8),1),c(mod(w-1,8)+1,1)]);
            end
            pow = pow+1;
            hold on;

        end
        xlabel('IMAG velocity eigen vectors for the tube');
        axis([0.0, L_tube-hdx -1 1]);
        hold off;
        
        tosave = input('sv+enter -save these Eigen Vectors; enter -no save required\n','s');
        if(strcmp(tosave,'sv'))
            saveas(fig2,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' ANNULUS_EV.fig']);
            saveas(fig3,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_real_EV.fig']);
            saveas(fig4,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_imag_EV.fig']);
        end
        alphabet = input('z+enter -change region; s+enter -stay on same region; Any Other key+enter -exit\n','s');
        close(fig2);
        close(fig3);
        close(fig4);
        clc;
    end
end
disp('exit');
close all;