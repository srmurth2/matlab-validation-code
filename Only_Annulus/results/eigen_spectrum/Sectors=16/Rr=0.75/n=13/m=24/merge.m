%=============================================================================================================================
% This script merges all MATLAB data in the current folder using the file names
% present in the file_list.txt file
% REQUIREMENTS : 
% This script must exist in a folder containing
%   1. polarplot3d                                                                (polar ploting)
%   2. create_full_pressure                                                       (constructs the full solution from the sector eigen vectors)
%   3. file_list.txt                                                              (file containing a list of all the MATLAB DATA files)
%   4. MATLAB DATA files with Eigen Vector and Eigen Value data for all j values  (all MATALB DATA files)
%=============================================================================================================================

clc;
close all;
clear all;
n_rj = 0;
w_t_l = input('Which j value to load, press enter for all : ');

if(isempty(w_t_l))

    file_names = importdata('file_list.txt');
    disp('Loading of j0: Started');
    tic
    load(char(file_names(1)));
    toc
    EV_Set_all = EV_Set;
    EV_af_all  = EV_af;
    num_eva_per_j = length(EV_af);
    disp('Loading of j0: Complete');
    for j = 2:length(file_names)
        disp(['Loading of j',num2str(j-1),' : Started']);
        load(char(file_names(j)));
        disp(['Loading of j',num2str(j-1),' : Complete']);
        EV_Set_all = [EV_Set_all, EV_Set];
        EV_af_all  = [EV_af_all; EV_af];
    end
    num_j = length(file_names);

else
    file_names = importdata('file_list.txt');
    disp(['Loading of j',num2str(w_t_l),': Started']);
    tic
    load(char(file_names(w_t_l+1)));
    toc
    EV_Set_all = EV_Set;
    EV_af_all  = EV_af;
    num_eva_per_j = length(EV_af);
    num_j = 1;
end
    

c = ['b';'r';'k';'m';'y';'b';'c';'g'];
shape = ['o';'s';'d';'+';'*';'<';'v';'x'];
alphabet = 'z';
trial=1;
while(alphabet == 'z')
    fig1=figure(1);
    if(trial == 1)
        hold on;
        grid on;
        for j = 1:num_j
            if(j<=7)
                plot(real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),[shape(mod(j-1,8)+1,1),c(mod(j-1,8)+1,1)]);
           
            else
                plot(real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),[shape(8-mod(j-1,8),1),c(mod(j-1,8)+1,1)]);
            end

        end
        axis([-1e-10 1e-10 -8.5 8.5])
        xlabel('Eigen Spectrum');
        hold off;
        trial=2;
    end

    disp('Select the appropriate region of interest then press Enter');
    pause;
    alphabet = 's';
    while(alphabet == 's')
       
        Dsel    = ginput(1);                        %..see ginput. [x, y] value selected
        Dsel    = Dsel(1) + 1i*Dsel(2);             %..putting result into complex value

        Ddiff   = abs(EV_af_all - Dsel);                   %..Distance from selected value to each eigenvalue
        [~, II] = min(Ddiff);                       %..determine minimum distance to find the nearest eigenvalue
        Dval    = EV_af_all(II);                           %..Assign selected eigenvalue to Dval (output

        V    = EV_Set_all(:, II);                           %..Find the eigenvector of the corresponding eigenvalue
        
        if(isempty(w_t_l))
           i = ceil(II/num_eva_per_j);
        else
            i = w_t_l+1;
        end

        % 2D ANNULUS Plot
        fig2 = figure(2);
        set(fig2,'Position',[0,0,2300,1300]);
        % l=l+1;
        subplot(1,2,1);
        full_theta = [];
        for h=1:S
        full_theta = [full_theta, (h-1)*theta*ones(1,length(theta_vec))+theta_vec];
        end
        
        % full domain REAL
        full_pressure = create_full_pressure(V((2*(n_rj)+1):end),S,i-1,m,'r');
        polarplot3d(full_pressure,'plottype','surfn','angularrange',full_theta,'radialrange',r_vec_p,'tickspacing',15);
        xlabel(['REAL pressure, J=',int2str(i-1),', ev = ',num2str(Dval),' and #sec = ',int2str(S),' with ',int2str(n),' r & ', int2str(m),' th divisions']);
        view(0,90);
        colorbar;
        axis square
        
        %   full domain IMAGINARY
        subplot(1,2,2);
        full_pressure = create_full_pressure(V((2*(n_rj)+1):end),S,i-1,m,'i');
        polarplot3d(full_pressure,'plottype','surfn','angularrange',full_theta,'radialrange',r_vec_p,'tickspacing',15);
        xlabel(['IMAG pressure, J=',int2str(i-1),', ev = ',num2str(Dval),' and #sec = ',int2str(S),' with ',int2str(n),' r & ', int2str(m),' th divisions']);
        view(0,90);
        colorbar;
        axis square
        
        %=================================================
        
        tosave = input('sv+enter -save these Eigen Vectors; enter -no save required\n','s');
        if(strcmp(tosave,'sv'))
            saveas(fig2,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' ANNULUS_EV.fig']);
%             saveas(fig3,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_real_EV.fig']);
%             saveas(fig4,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_imag_EV.fig']);
        end
        alphabet = input('z+enter -change region; s+enter -stay on same region; Any Other key+enter -exit\n','s');
        close(fig2);
%         close(fig3);
%         close(fig4);
        clc;
    end
end
disp('exit');
close all;