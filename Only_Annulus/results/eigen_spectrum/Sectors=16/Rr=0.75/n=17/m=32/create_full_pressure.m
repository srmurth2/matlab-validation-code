function f_p = create_full_pressure(I_all,S,i,m,r_or_i)

c = cell(1,S);

I_all_r = reshape(I_all,[],m);

for j=0:S-1
    c{1,j+1} = exp(sqrt(-1)*(j)*(i)*2*pi/S)*I_all_r;
end

if(strcmp(r_or_i,'r'))
    f = real(cell2mat(c));
else
    f = imag(cell2mat(c));
end

f_r = reshape(f(1:3:end,:),[],1);

f_p = reshape(f_r,[],S*m);

end