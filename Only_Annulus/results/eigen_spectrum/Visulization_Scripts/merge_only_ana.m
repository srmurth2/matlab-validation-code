%=============================================================================================================================
% This script merges all MATLAB data in the current folder using the file names
% present in the file_list.txt file
% REQUIREMENTS : 
% This script must exist in a folder containing
%   1. polarplot3d                                                                (polar ploting)
%   2. create_full_pressure                                                       (constructs the full solution from the sector eigen vectors)
%   3. file_list.txt                                                              (file containing a list of all the MATLAB DATA files)
%   4. MATLAB DATA files with Eigen Vector and Eigen Value data for all j values  (all MATALB DATA files)
%=============================================================================================================================

clc;
close all;
clear all;

file_names = importdata('file_list.txt');
load(char(file_names(1)));

EV_Set_1 = EV_Set; EV_af_1  = EV_af;
num_eva_per_j = length(EV_af);

num_j = 2;
beta = 16;  
alphabet = 'z';
trial=1;
while(alphabet == 'z')
    fig1=figure(1);
    if(trial == 1)
        hold on;
        grid on;
        plot(real(EV_af_1),imag(EV_af_1),'ob');
        xlabel('Eigen Spectrum');
        
        EV_af_3 = analytical_sol_only_annulus([-70 70], beta , R2, R1);
        
        Apply_analytical_result;
        
        hold off;
        axis([-0.1 0.1 -20 20]);
        trial=2;
    end

    disp('Select the appropriate region of interest then press Enter');
    pause;
    alphabet = 's';
    while(alphabet == 's')
      
        % eigen value of the analytical solution
        Dsel3    = ginput(1);                        %..see ginput. [x, y] value selected
        Dsel3    = Dsel3(1) + 1i*Dsel3(2);             %..putting result into complex value

        Ddiff3   = abs(EV_af_3 - Dsel3);            %..Distance from selected value to each eigenvalue
        [~, II3] = min(Ddiff3);                       %..determine minimum distance to find the nearest eigenvalue
        Dval3    = EV_af_3(II3);                    %..Assign selected eigenvalue to Dval (output

      
         %% 2D ANNULUS Plot from the full annulus analytical
        fig4 = figure(4);
        set(fig4,'Position',[0,0,2300,1300]);
        % l=l+1;
        subplot(1,2,1);
        full_theta = [];
        for h=1:S
        full_theta = [full_theta, (h-1)*theta*ones(1,length(theta_vec))+theta_vec];
        end
        
        [RR, TT] = meshgrid(-besselj(beta, imag(Dval3)*r_vec_p) * (bessely(beta-1,imag(Dval3)*R1) - bessely(beta+1,imag(Dval3)*R1)) + bessely(beta, imag(Dval3)*r_vec_p) *((besselj(beta-1,imag(Dval3)*R1) - besselj(beta+1,imag(Dval3)*R1))),...
            exp(sqrt(-1)*16*full_theta));
        pre = RR.*fliplr(TT);
       
        % full domain REAL
        full_pressure = real(pre');
        polarplot3d(full_pressure,'plottype','surfn','angularrange',full_theta,'radialrange',r_vec_p,'tickspacing',15);
        xlabel(['REAL pressure, nu=',num2str(beta),', ev = ',num2str(Dval3),' and #sec = ',int2str(1),' with ',int2str(n),' r & ', int2str(m*beta),' th divisions']);
        view(0,90);
        colorbar;
        axis square
        
        % full domain IMAGINARY
        subplot(1,2,2);
        full_pressure = imag(pre');
        polarplot3d(full_pressure,'plottype','surfn','angularrange',full_theta,'radialrange',r_vec_p,'tickspacing',15);
        xlabel(['IMAG pressure, nu=',num2str(beta),', ev = ',num2str(Dval3),' and #sec = ',int2str(1),' with ',int2str(n),' r & ', int2str(m*beta),' th divisions']);
        view(0,90);
        colorbar;
        axis square
        %==================================================
        
        figure(10);
        plot(-besselj(beta, imag(Dval3)*r_vec_p) * ((bessely(beta-1,imag(Dval3)*R1) - bessely(beta+1,imag(Dval3)*R1))) + bessely(beta, imag(Dval3)*r_vec_p)*(besselj(beta-1,imag(Dval3)*R1) - besselj(beta+1,imag(Dval3)*R1)));
        
        
        tosave = input('sv+enter -save these Eigen Vectors; enter -no save required\n','s');
        if(strcmp(tosave,'sv'))
            saveas(fig4,['./saved_eigenvectors/nu=',num2str(beta),' eig_val = ',num2str(Dval3),' ANNULUS_EV_analytical.fig']);
        end
        alphabet = input('z+enter -change region; s+enter -stay on same region; Any Other key+enter -exit\n','s');
        close(fig4);
        clc;
    end
end
disp('exit');
close all;