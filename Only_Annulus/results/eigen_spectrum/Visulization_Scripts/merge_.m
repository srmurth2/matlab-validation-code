%=============================================================================================================================
% This script merges all MATLAB data in the current folder using the file names
% present in the file_list.txt file
% REQUIREMENTS : 
% This script must exist in a folder containing
%   1. polarplot3d                                                                (polar ploting)
%   2. create_full_pressure                                                       (constructs the full solution from the sector eigen vectors)
%   3. file_list.txt                                                              (file containing a list of all the MATLAB DATA files)
%   4. MATLAB DATA files with Eigen Vector and Eigen Value data for all j values  (all MATALB DATA files)
%=============================================================================================================================

clc;
close all;
clear all;

file_names = importdata('file_list.txt');
load(char(file_names(1)));

EV_Set_all = EV_Set; EV_af_all  = EV_af;
EV_Set_1 = EV_Set; EV_af_1  = EV_af;
num_eva_per_j = length(EV_af);

num_j = 2;
    
c = ['b';'r';'k';'m';'y';'b';'c';'g'];
shape = ['o';'s';'d';'+';'*';'<';'v';'x'];
alphabet = 'z';
trial=1;
while(alphabet == 'z')
    fig1=figure(1);
    if(trial == 1)
        hold on;
        grid on;
        for j = 1:num_j
            plot(real(EV_af_1),imag(EV_af_1),'ob');
        end
        xlabel('Eigen Spectrum');
        
        Apply_analytical_result;
        
        hold off;
        trial=2;
    end

    disp('Select the appropriate region of interest then press Enter');
    pause;
    alphabet = 's';
    while(alphabet == 's')
       
        % eigen value of the root of unity spectrum is selected
        Dsel    = ginput(1);                        %..see ginput. [x, y] value selected
        Dsel    = Dsel(1) + 1i*Dsel(2);             %..putting result into complex value

        Ddiff   = abs(EV_af_1 - Dsel);            %..Distance from selected value to each eigenvalue
        [~, II] = min(Ddiff);                       %..determine minimum distance to find the nearest eigenvalue
        Dval    = EV_af_1(II);                    %..Assign selected eigenvalue to Dval (output

        V    = EV_Set_1(:, II);                   %..Find the eigenvector of the corresponding eigenvalue
        
        i = rou;

        %% 2D ANNULUS Plot from the jth root of unity solve
        fig2 = figure(2);
        set(fig2,'Position',[0,0,2300,1300]);
        % l=l+1;
        subplot(1,2,1);
        full_theta = [];
        for h=1:S
        full_theta = [full_theta, (h-1)*theta*ones(1,length(theta_vec))+theta_vec];
        end
        
        % full domain REAL
        full_pressure = create_full_pressure(V,S,i-1,m,'r');
        polarplot3d(full_pressure,'plottype','surfn','angularrange',full_theta,'radialrange',r_vec_p,'tickspacing',15);
        xlabel(['REAL pressure, J=',int2str(i-1),', ev = ',num2str(Dval),' and #sec = ',int2str(S),' with ',int2str(n),' r & ', int2str(m),' th divisions']);
        view(0,90);
        colorbar;
        axis square
        
        % full domain IMAGINARY
        subplot(1,2,2);
        full_pressure = create_full_pressure(V,S,i-1,m,'i');
        polarplot3d(full_pressure,'plottype','surfn','angularrange',full_theta,'radialrange',r_vec_p,'tickspacing',15);
        xlabel(['IMAG pressure, J=',int2str(i-1),', ev = ',num2str(Dval),' and #sec = ',int2str(S),' with ',int2str(n),' r & ', int2str(m),' th divisions']);
        view(0,90);
        colorbar;
        axis square
        %==================================================

        tosave = input('sv+enter -save these Eigen Vectors; enter -no save required\n','s');
        if(strcmp(tosave,'sv'))
            saveas(fig2,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' ANNULUS_EV_jth_rou.fig']);
        end
        alphabet = input('z+enter -change region; s+enter -stay on same region; Any Other key+enter -exit\n','s');
        close(fig2);
        clc;
    end
end
disp('exit');
close all;