% function constructs the H_j matrix at the given j value (wave_num)

function [R_Eve_j, Eva] = eigenVSolve(wave_num)
  % required variables
  global S cm cm2 cmn
  
  disp('Started EVsolve');
  
  % root of unity
  rou  = exp(sqrt(-1)*(wave_num)*2*pi/S);
  
  % construction of the H_j matrix      
  H_j = (cmn./rou + cm + cm2.*rou);
  
  tic
  [R_Eve_j, Eva] = eig(H_j);
  toc
  
  disp('Ended EVsolve, after');
  
  disp('Co-efficient of the 2nd highest order term in the charecteristic polynomial');
  sum(diag(Eva))
 end