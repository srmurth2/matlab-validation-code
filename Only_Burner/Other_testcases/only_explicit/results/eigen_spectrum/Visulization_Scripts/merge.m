%=============================================================================================================================
% This script merges all MATLAB data in the current folder using the file names
% present in the file_list.txt file
% REQUIREMENTS : 
% This script must exist in a folder containing
%   1. polarplot3d                                                                (polar ploting)
%   2. create_full_pressure                                                       (constructs the full solution from the sector eigen vectors)
%   3. file_list.txt                                                              (file containing a list of all the MATLAB DATA files)
%   4. MATLAB DATA files with Eigen Vector and Eigen Value data for all j values  (all MATALB DATA files)
%=============================================================================================================================

clc;
close all;
clear all;

disp(' ALL STUFF WORKING : ');

file_names = importdata('file_list.txt');
disp('Loading of j0: Started');
tic
load(char(file_names(1)));
toc
EV_Set_all = EV_Set;
EV_af_all  = EV_af;
num_eva_per_j = length(EV_af);
disp('Loading of j0: Complete');
for j = 2:length(file_names)
    disp(['Loading of j',num2str(j-1),' : Started']);
    load(char(file_names(j)));
    disp(['Loading of j',num2str(j-1),' : Complete']);
    EV_Set_all = [EV_Set_all, EV_Set];
    EV_af_all  = [EV_af_all; EV_af];
end

c = ['b';'r';'k';'m';'y';'b';'c';'g'];
shape = ['o';'s';'d';'+';'*';'<';'v';'x'];
alphabet = 'z';
trial=1;
while(alphabet == 'z')
    fig1=figure(1);
    if(trial == 1)
        hold on;
        grid on;
        for j = 1:length(file_names)
            if(j<=7)
                real_vec = real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j));
                imag_vec = imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j));
                indP = find(real_vec>1e-14);
                indN = find(real_vec<=1e-14);
                plot(real_vec(indP),imag_vec(indP),'or','MarkerSize',10);
                plot(real_vec(indN),imag_vec(indN),'ob');
                disp(size(indP));

            else
                plot(real(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),imag(EV_af_all((j-1)*num_eva_per_j+1:j*num_eva_per_j)),[shape(8-mod(j-1,8),1),c(mod(j-1,8)+1,1)]);
            end

        end
        xlabel('Eigen Spectrum');
        hold off;
        trial=2;
    end

    disp('Select the appropriate region of interest then press Enter');
    pause;
    alphabet = 's';
    while(alphabet == 's')
       
        Dsel    = ginput(1);                        %..see ginput. [x, y] value selected
        Dsel    = Dsel(1) + 1i*Dsel(2);             %..putting result into complex value

        Ddiff   = abs(EV_af_all - Dsel);                   %..Distance from selected value to each eigenvalue
        [~, II] = min(Ddiff);                       %..determine minimum distance to find the nearest eigenvalue
        Dval    = EV_af_all(II);                           %..Assign selected eigenvalue to Dval (output

        V    = EV_Set_all(:, II);                           %..Find the eigenvector of the corresponding eigenvalue
        
        i = ceil(II/num_eva_per_j);
        
        % Tube plots
        
        fig3 = figure(3);
        set(fig3,'Position',[0,0,600,600]);
        pow = 0;
        for w = 1:1
            % full domain REAL Rijke tube
            if(w<=7)
                plot(XU.',([0.0; real(V(2:2:end))]...
                /norm([0.0; (V(2:2:end))])),['-',shape(mod(w-1,8)+1,1),c(mod(w-1,8)+1,1)]);
            else
                plot(XU.',([0.0; real(V(2:2:end))]...
                /norm([0.0; (V(2:2:end))])),['-',shape(8-mod(w-1,8),1),c(mod(w-1,8)+1,1)]);
            end
            pow = pow+1;
            hold on;

        end
        xlabel(['REAL velocity eigen vectors for the tube eig val = ',num2str(Dval)]);
        axis([0.0, L_tube-hdx, -1, 1]);
        hold off;
        
        % TUBE IMAG 
        fig4 = figure(4);
        set(fig4,'Position',[0,0,600,600]);
        pow = 0;
        for w = 1:1

            % full domain IMAG Rijke tube
            if(w<=7)
                plot(XU.',([0.0; imag(V(2:2:end))]...
                /norm([0.0; (V(2:2:end))])),['-',shape(mod(w-1,8)+1,1),c(mod(w-1,8)+1,1)]);
            else
                plot(XU.',([0.0; imag(V(2:2:end))]...
                /norm([0.0; (V(2:2:end))])),['-',shape(8-mod(w-1,8),1),c(mod(w-1,8)+1,1)]);
            end
            pow = pow+1;
            hold on;

        end
        xlabel(['IMAG velocity eigen vectors for the tube eig val = ',num2str(Dval)]);
        axis([0.0, L_tube-hdx, -1, 1]);
        hold off;
        
        % Pressure
        fig5 = figure(5);
        set(fig5,'Position',[0,0,600,600]);
        pow = 0;
        for w = 1:1
            % full domain REAL Rijke tube
            if(w<=7)
                plot(XP.',([real(V(1:2:end)); 0]...
                /norm([(V(1:2:end)); 0])),['-',shape(mod(w-1,8)+1,1),c(mod(w-1,8)+1,1)]);
            else
                plot(XP.',([real(V(1:2:end)); 0.0]...
                /norm([(V(1:2:end)); 0.0])),['-',shape(8-mod(w-1,8),1),c(mod(w-1,8)+1,1)]);
            end
            pow = pow+1;
            hold on;

        end
        xlabel(['REAL pressure eigen vectors for the tube eig val = ',num2str(Dval)]);
        axis([hdx, L_tube, -1, 1]);
        hold off;
        
        % TUBE IMAG 
        fig6 = figure(6);
        set(fig6,'Position',[0,0,600,600]);
        pow = 0;
        for w = 1:1

            % full domain IMAG Rijke tube
            if(w<=7)
                plot(XP.',([imag(V(1:2:end)); 0]...
                /norm([(V(1:2:end)); 0])),['-',shape(mod(w-1,8)+1,1),c(mod(w-1,8)+1,1)]);
            else
                plot(XP.',([imag(V(1:2:end)); 0.0]...
                /norm([(V(1:2:end)); 0.0])),['-',shape(8-mod(w-1,8),1),c(mod(w-1,8)+1,1)]);
            end
            pow = pow+1;
            hold on;

        end
        xlabel(['IMAG pressure eigen vectors for the tube eig val = ',num2str(Dval)]);
        axis([hdx, L_tube, -1, 1]);
        hold off;
        
        tosave = input('sv+enter -save these Eigen Vectors; enter -no save required\n','s');
        if(strcmp(tosave,'sv'))
            saveas(fig3,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_real_V_EV.fig']);
            saveas(fig4,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_imag_V_EV.fig']);
            saveas(fig5,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_real_P_EV.fig']);
            saveas(fig6,['./saved_eigenvectors/j = ',num2str(i-1),' eig_val = ',num2str(Dval),' TUBE_imag_P_EV.fig']);
        end
        alphabet = input('z+enter -change region; s+enter -stay on same region; Any Other key+enter -exit\n','s');
        close(fig3);
        close(fig4);
        close(fig5);
        close(fig6);
        clc;
    end
end
disp('exit');
close all;