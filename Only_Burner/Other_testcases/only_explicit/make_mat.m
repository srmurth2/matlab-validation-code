function A = make_mat(Y)

  global iS K dx
  global gamma Ma lag
  global M H DAMPING

  y_size = size(Y);
  H1 = zeros(y_size(1,2),y_size(1,2));
  
  % Shock strength
  vec = zeros(1,y_size(1,2));
  vec(1,1 + 2*(iS-1) - 1) = 0.5;
  vec(1,1 + 2*(iS-1) + 1) = 0.5;
  
  H1(1 + 2*(iS-1),:) = H1(1 + 2*(iS-1),:) + gamma*Ma*K*0.25*(sqrt(3))*(vec)/dx;
  
  % The portion of the q vector that needs to be delayed
  H1 = M\H1;
  
  % Explicit, impplicit and Damping matrcies are used to construct Ai
  Ai = -M\H - DAMPING;
  
  % DDE 
  disp('Performing DDE as per thesis');
  N = 13;                                                                                          % number of points in the theta direction
  Diff = -cheb(N-1)*2/lag;                                                                         % find the cheb
  A_N = [kron(Diff(1:N-1,:), eye(y_size(1,2)));[H1, zeros(y_size(1,2),(N-2)*y_size(1,2)), Ai]];    % Define A_N such that each level comes closer to the considering the total lag tau
  [EVe_Set, EVa_Set] = eig(A_N);                                                                   % find the eigen vectors and eigen values of A_N
  sub_EVe_Set = EVe_Set(y_size(1,2)*(N-1)+1:end,:);                                                % Subset of egien vectors that represent the nature of the solution at the derised time t
  A = (sub_EVe_Set*EVa_Set/sub_EVe_Set);                                                           % Construction of new matrix, which now incorporates the lag effect
  
%   A = M\A;
  
%   A =  A - DAMPING;

end