function DAMPING = damping_rijke()
    global n_rj
    
    nt = n_rj;
    DAMPING = zeros(2*nt,2*nt);

    for i=1:nt
        for j=1:nt
            for k=1:nt
                DAMPING(P2G(i),P2G(j)) = DAMPING(P2G(i),P2G(j)) + DAMP(k)       ...
                    * cos(pi*(i-0.5)*(k-0.5)/(nt+0.5)) ...
                    * cos(pi*(k-0.5)*(j-0.5)/(nt+0.5));
            end
        end
    end
    DAMPING = 2.0*DAMPING/(nt+0.5);

end

function [g] = P2G(p)
    g = 2*(p-1)+1;
end

function [xik] = DAMP(k)

  % Input: integer k
  % Based on JFM paper by Sujith, dependence on tube length drops out

  global c1 c2
  xik = c1*k.^2+c2.*sqrt(k);

end