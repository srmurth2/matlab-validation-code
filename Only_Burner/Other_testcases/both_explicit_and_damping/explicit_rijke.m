function A_rj = explicit_rijke()

  global dx 
  global gamma Ma n_rj
  
  nt=n_rj;
  
    % ------------- %
    % Explicit part %
    % ------------- %
    
    D = [-1, +1];
    B = zeros(2*nt,3);
    
    % Lower diagonal
    d = 1; s = D(d);
    for i=2:2*nt
        B(i+s,d) = - 1 / dx;
    end
    
    % Upper diagonal
    d = 2; s = D(d);
    for i=1:2*nt-1
        B(i+s,d) = + 1 / dx;
    end
    
    A_rj = spdiags(B,D,2*nt,2*nt);
    
    % Velocity gradient
    for i=2:nt+1
        A_rj(U2G(i),:) = A_rj(U2G(i),:) / gamma / Ma;
    end
    
    % Pressure gradient
    for i=1:nt
        A_rj(P2G(i),:) = A_rj(P2G(i),:) * gamma * Ma;
    end
 
end

function [g] = P2G(p)
    g = 2*(p-1)+1;
end

function [g] = U2G(u)
    g = 2*u-2;
end