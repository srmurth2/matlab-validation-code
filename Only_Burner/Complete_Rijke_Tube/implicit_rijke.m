function M = implicit_rijke()
  global n_rj
  global pade_ 
  nt=n_rj;

    % ------------- %
    % Implicit part %
    % ------------- %
    D = [-2,0,+2];
    B = zeros(2*nt,3);
    
    % Lower diagonal
    d = 1; s = D(d);
    for i=3:2*nt-1
        B(i+s,d) = pade_;
    end
    
    % Main diagonal
    d = 2; s = D(d);
    i = 1; B(i+s,d) = 1.0;
    for i=2:2*nt-1
        B(i+s,d) = 1.0-2.0*pade_;
    end
    i = 2*nt; B(i+s,d) = 1.0;
    
    % Upper diagonal
    d = 3; s = D(d);
    for i=2:2*nt-2
        B(i+s,d) = pade_;
    end
    
    % Create (sparse) mass matrix
    M = spdiags(B,D,2*nt,2*nt);


end