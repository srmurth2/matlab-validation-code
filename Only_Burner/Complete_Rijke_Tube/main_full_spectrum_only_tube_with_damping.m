%% WORKSPACE INITIALIZATION
   
    clc;
    clear all;
    close all;
    addpath './pcheb'
    
    % ---------- %
    % User input %
    % ---------- %  
    
    % factor (only odd numbers)
    fac = 1; 
    
    % Rijke Tube geometry
    
    global L_tube; L_tube = 1;                                % Length of Rijke Tube
    global n_rj;   n_rj   = 64*fac+floor(fac/2);              % Number of points inside the domain. Points at which we compute variables
    global dx;	  dx      = (L_tube/(n_rj+0.5));			  % dx
    global hdx;	  hdx     = 0.5*dx;				              % half dx
                                            
    % Thermodynamic properties
    global gamma; gamma = 1.4;
    global Ma;    Ma    = 0.05;
    global c1;    c1    = 0.1;                               % Damping - linear coef
    global c2;    c2    = 0.06;                              % Damping - inverse SQRT coef
    global K;     K     = 1.15;                                % Shock strength
    global lag;   lag   = 0.3;                                % Heater delay

    % ----------------- % 
    % Global parameters %
    % ----------------- %

    global sh;     sh     = -1;                               % Shift (matlab indexing)
    global pade_; pade_   = 1.0/24.0;                         % Off-diagonal coefficient
    
    % x co-ordinates
    global XU;    XU  = linspace(0.0, L_tube-hdx,n_rj+1);     % Velocitoes co-ordinates 
    global XP;    XP  = linspace(hdx, L_tube    ,n_rj+1);     % Pressure co-ordinates
        
    % -------------- %
    % Shock location %
    % -------------- %
    
    global iS; iS = 32*fac-floor(fac/2);                                       % Shock Location
    disp(['Flame located at ', num2str(XP(iS))]);
    
    % The total number of distinct quantities that ought to be calculated inside the Tube
    Y_rj = zeros(1, 2*(n_rj));
    
    %% Constant matrices in the code
    
    tic
    global M  
    M = implicit_rijke();
    disp('Mass matrix done, after');
    toc
    disp('============================');
    tic
    global H  
    H = explicit_rijke();
    disp('Explicit matrix done, after');
    toc
    disp('============================');
    
    global DAMPING
    tic
    DAMPING = damping_rijke();
    disp('Damping matrix done, after');
    toc
    disp('============================');    
    
    %% Creates required directories, files and clears file_list

    if(exist(['./results/eigen_spectrum/K=',num2str(K),'/n_rj=',num2str(n_rj),'/tau=',num2str(lag),'/'],'dir')==0)
        mkdir(['./results/eigen_spectrum/K=',num2str(K),'/n_rj=',num2str(n_rj),'/tau=',num2str(lag),'/saved_eigenvectors']);
        copyfile('./results/eigen_spectrum/Visulization_Scripts/merge.m',['./results/eigen_spectrum/K=',num2str(K),'/n_rj=',num2str(n_rj),'/tau=',num2str(lag)]);    
    end
    fid=fopen(['./results/eigen_spectrum/K=',num2str(K),'/n_rj=',num2str(n_rj),'/tau=',num2str(lag),'/file_list.txt'],'w');

    %% COMPUTING THE EIGEN VALUES/VECTORS FOR A SECTOR

    i=1;
        [EV_Set, EVa_Set] = eig(make_mat(Y_rj)); % Tube code is confirmed and 2D annulus is confirmed
        EV_af = diag(EVa_Set);

        % Save eigen right eigen vector matrix and eigen value vector        
        if(i==1)
            save(['./results/eigen_spectrum/K=',num2str(K),'/n_rj=',num2str(n_rj),'/tau=',num2str(lag),'/j',num2str(i-1),'_K_',num2str(K),'_tau_',num2str(lag),'_EV_Set_EV_af.mat'],...
                'EV_Set','EV_af','n_rj','XP','XU','hdx','L_tube'); 
           
            fprintf(fid,'%s',['j',num2str(i-1),'_K_',num2str(K),'_tau_',num2str(lag),'_EV_Set_EV_af.mat']);
        else
            save(['./results/eigen_spectrum/K=',num2str(K),'/n_rj=',num2str(n_rj),'/tau=',num2str(lag),'/j',num2str(i-1),'_K_',num2str(K),'_tau_',num2str(lag),'_EV_Set_EV_af.mat'],...
                'EV_Set','EV_af','n_rj','XP','XU','hdx','L_tube');
            fprintf(fid,'\n%s',['j',num2str(i-1),'_K_',num2str(K),'_tau_',num2str(lag),'_EV_Set_EV_af.mat']);
        end
        disp(['==========EigenVectors and EigenValues of j = ',int2str(i-1),' saved==================']);               
    
    
    fclose(fid);
    
    disp('===========DONE==============');